# asimov

Web de Asimov, Juego Modular Colaborativo

[ASIMOV/](https://asimov.copiona.com)
#modulargame


---
La web de asimov es un template Designed by Yebo Logo && Developed by Peter Finlan sacado de [aquí](https://htmltemplates.co/free-html5-templates/yebo-free-responsive-html5-bootstrap-template)

La información contenida en la web y los modelos 3D junto con el desarollo de diseño está bajo la [licencia de producción de pares](https://endefensadelsl.org/ppl_es.html)

[link](https://www.flickr.com/photos/boedker/4579453630/) imagen inicio by Mads Bødker
